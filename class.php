<?php
    class Htmlpage {
        protected $title = "Exercise 3";
        protected $body = "";
        function __construct($title ="",$body ="") {
            if ($title !=""){
                $this->title=$title;
            }
            if ($body !=""){
                $this->body=$body;
            }
         }         
        public function view() {
            echo "<html>
                  <head><title>$this->title</title></head>
                  <body>$this->body</body>
                  </html>
                ";
        }  
    }
        
    class ColoredMessage extends Htmlpage {
      protected $color = 'blue';
      public function __set($property,$value){
        if ($property == 'color'){
          $colors = array('blue','red','green','pink');
          if(in_array($value, $colors)){
            $this->color = $value;
          }
          else{
            $this->body = 'Error! this color is illegal';
          }
        }
      }

    public function show(){
      echo "<p style = 'color:$this->color'>$this->body</p>";
    }
  }

  class FontSize extends ColoredMessage {
    protected $size;
    public function __set($property,$value){
        if($property == 'size'){
            $size = range(10,24);
            if(in_array($value,$size)){
                $this->size = $value;
            }elseif($this->body == 'Error! this color is illegal'){
                $this->body = 'Error! this color and this font-size are illegal';
            }else {
                $this->body = 'Error! this font-size is illegal';
            }
        }
        elseif($property = 'color') {
            parent::__set($property,$value);
          }
      }
    public function show1(){
    echo "<p style = 'color:$this->color;font-size:$this->size'>$this->body</p>";
    }
  }
  
?>


